#!/bin/bash

#
# 配置方式：
#	1：安装mongodb-clients,以便使用mongodump命令(Ubuntu下：yes|apt install mongodb-clients)(Centos下：yum -y install mongodb-clients)
#   1:上传配置文件以及执行文件至目录/etc/mongodbbackup
#   2:依照当前系统环境修改mongodbbackup.conf,配置待导出数据信息mongodbbackup_schemas.conf
#   3:设置可执行权限  chmod +x /etc/mongodbbackup/mongodbbackup.sh
#   4:配置定时任务,查看命令：crontab -l, 命令：crontab -e 进入编辑状态添加(每天凌晨1点自动执行): 0 1 * * * bash /etc/mongodbbackup/mongodbbackup.sh isauto=true 
#   5:手动备份方式：bash /etc/mongodbbackup/mongodbbackup.sh limitschemas=xxx1,xx2
# 	7: mongodump -h IP --port 端口 -u 用户名 -p 密码 -d 数据库 -c 数据集 -o 文件存放路径
#

# 加载配置
source /etc/mongodbbackup/mongodbbackup.conf

PATH=$PATH:$MONGODB_TOOLS_BIN_PATH
export PATH

# 创建目录
mkdir -p $MONGODB_EXPDATA_DIR
mkdir -p $LOG_DIR

# 获取参数(最多三个参数)
# 参数参考
# 1：limitschemas=xxx1,xx2
# 2: isauto=true
# 3: 没想好再说吧
# 示例：sh /etc/mongodbbackup/mongodbbackup.sh limitschemas=xxx1,xx2 isauto=true

if [ "$#" -gt 0 ];then 
    for arg in $*
    do
        if [ -z "$arg" ] ;then
            continue
        fi
        argStr=${arg//=/ }
        argArr=()
        subArgCount=0
        for subArg in $argStr   
        do
            argArr[subArgCount]=$subArg
            ((subArgCount++))
        done
        if [ "$subArgCount" -ne 2 ]; then
            continue
        fi
        if [ "${argArr[0]}" = "limitschemas" ] ;then
             limitschemas=${argArr[1]}
        elif [ "${argArr[0]}" = "isauto" ] ;then
            isauto=${argArr[1]}
        fi
    done
fi

# 定义操作类型
operation_type="手动"
if [ "$isauto" = true ]; then
    operation_type="自动"
fi

# 获取已定义的导出数据库

schemasArr=()
groupArr=()
schemasCount=0
groupCount=0
while read line
do
    schemasline=$line
    if [[ -z $schemasline || $schemasline =~ ^# ]] ;then
        continue
    fi
    singleExpStr=${schemasline//// }
    singleExpCount=0
    for singleExp in $singleExpStr   
    do  
        singleExpArr[$singleExpCount]=$singleExp
        ((singleExpCount++))
    done
    db_group=""
	if [ "$singleExpCount" -eq 1 ]; then
        schemas=${singleExpArr[0]}
        singleExpStr="$MONGODB_ADMIN_AUTH $MONGODB_ADMIN_USER $MONGODB_ADMIN_PASSWORD $singleExpStr"
    elif [ "$singleExpCount" -eq 2 ]; then
        schemas=${singleExpArr[0]}
        singleExpStr="$MONGODB_ADMIN_AUTH $MONGODB_ADMIN_USER $MONGODB_ADMIN_PASSWORD $singleExpStr"
        db_group=${singleExpArr[1]}
    elif [ "$singleExpCount" -eq 3 ]; then
        schemas=${singleExpArr[2]}
		singleExpStr="$schemas $singleExpStr"
    elif [ "$singleExpCount" -eq 4 ]; then
        schemas=${singleExpArr[2]}
		singleExpStr="$schemas $singleExpStr"
        db_group=${singleExpArr[3]}
    fi
	if [ -z $schemas ] ;then
        continue
    fi
    if [ -n "$limitschemas" ];then
        matchlimit=$(echo $limitschemas | grep "${schemas}")
        if [ -z "$matchlimit" ];then
            continue
        fi
    fi
    if [ -n "$db_group" ];then
        groupArr[groupCount]=$db_group
        ((groupCount++))
    fi
    schemasArr[$schemasCount]=$singleExpStr
    ((schemasCount++))
done < $DB_SCHEMAS_CONF

schemasNum=${#schemasArr[@]}
if [ "$schemasNum" -eq 0 ]; then
   exit 1
fi

#去重
groupNum=${#groupArr[@]}
if [ "$groupNum" -gt 1 ]; then
    groupArr=($(awk -v RS=' ' '!a[$1]++' <<< ${groupArr[@]}))
fi


#获取线程数
if [[ -z $THREADCOUNT || $THREADCOUNT -eq 0 ]]; then
    THREADCOUNT=1
fi
if [ $schemasNum -lt $THREADCOUNT ]; then
    THREADCOUNT=$schemasNum
fi

#创建线程管道
[ -e /tmp/mongodbbackup.fifo ] || mkfifo /tmp/mongodbbackup.fifo
exec 2<>/tmp/mongodbbackup.fifo
rm -rf /tmp/mongodbbackup.fifo
for ((i = 1 ; i <= $THREADCOUNT ; i++ ))
do
    echo >&2
done


# 开始执行操作
START_TIME=`date +%s`
CUR_DIR=`pwd`
cd $MONGODB_EXPDATA_DIR
touch $LOG_SINGLE_FILE
LOG_SINGLE_SIMPLE_FILE=${LOG_SINGLE_FILE}_simple
touch $LOG_SINGLE_SIMPLE_FILE
echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "***********************************************************************" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 执行${operation_type}备份数据库并压缩开始... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "***********************************************************************" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 启动多线程备份数据(共启动${THREADCOUNT}个线程备份${schemasNum}个数据库)开始... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
if [[ -n "$limitschemas" || -n "$isauto" ]];then
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 传入参数为：[limitschemas=$limitschemas] | [isauto=$isauto] ... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
fi
for ((i = 1 ; i <= $schemasNum ; i++))
do
read -u2
{
    singleExpStr=${schemasArr[i-1]}
    singleExpCount=0
    for singleExp in $singleExpStr   
    do  
        singleExpArr[$singleExpCount]=$singleExp
        ((singleExpCount++))
    done
	db_auth=${singleExpArr[0]}
    db_user=${singleExpArr[1]}
    db_password=${singleExpArr[2]}
    db_schemas=${singleExpArr[3]}
    #子路径
    SUB_MONGODB_BACKUPDATA_DIR=$MONGODB_BACKUPDATA_DIR
    if [ "${#singleExpArr[@]}" -eq 5 ]; then
        db_group=${singleExpArr[4]}
        SUB_MONGODB_BACKUPDATA_DIR=$SUB_MONGODB_BACKUPDATA_DIR/$db_group
    fi
    mkdir -p $SUB_MONGODB_BACKUPDATA_DIR
    unset singleExpArr
    
    touch ${LOG_SINGLE_FILE}_${i}
    touch ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo " " | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 开始备份(第${i}个)数据库：$db_schemas ..." | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    
        $MONGODB_TOOLS_BIN_PATH/$EXP_COMMAND --host $MONGODB_IP --port $MONGODB_PORT --authenticationDatabase=$db_auth -u $db_user -p $db_password --db $db_schemas --gzip --out $MONGODB_EXPDATA_DIR/${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1}  2>&1 | tee -a ${LOG_SINGLE_FILE}_${i}
        echo " 使用导出命令：$EXP_COMMAND , 用户：$db_user , 导出数据库：$db_schemas , 导出目录为： $MONGODB_EXPDATA_DIR ..." | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
 
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 备份成功：$MONGODB_EXPDATA_DIR/${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1} ..." | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 开始压缩文件：$MONGODB_EXPDATA_DIR/${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1} -> tar.gz ..." | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}    
        tar -czvf $MONGODB_EXPDATA_DIR/${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1}.tar.gz ${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1} 2>&1 | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 压缩成功：$MONGODB_EXPDATA_DIR/${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1}.tar.gz ..." | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 转移备份文件，原始位置：$MONGODB_EXPDATA_DIR/${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1}.tar.gz ..." | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
        mv $MONGODB_EXPDATA_DIR/${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1}.tar.gz $SUB_MONGODB_BACKUPDATA_DIR/${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1}.tar.gz 2>&1 | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 转移备份文件，当前位置：$SUB_MONGODB_BACKUPDATA_DIR/${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1}.tar.gz ..." | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 删除当前数据库备份文件：$MONGODB_EXPDATA_DIR/${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1} ..." | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}    
        rm -rf $MONGODB_EXPDATA_DIR/${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1} 2>&1 | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 成功删除当前数据库备份文件：$MONGODB_EXPDATA_DIR/${EXP_COMMAND}_${db_schemas}_${TIME_FORMAT_1} ..." | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo "-----------------------------------------------------------------------" | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    echo " " | tee -a ${LOG_SINGLE_FILE}_${i} | tee -a ${LOG_SINGLE_SIMPLE_FILE}_${i}
    #将令牌放回管道
    echo >&2
}&
done
wait
exec 2<&-                       
exec 2>&-

# 合并日志
echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 开始合并导出日志... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
for ((i = 1 ; i <= $schemasNum ; i++))
do
    echo "-----------------------------------------------------------------------" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 开始合并导出日志: ${LOG_SINGLE_FILE}_${i}... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-----------------------------------------------------------------------" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    cat ${LOG_SINGLE_FILE}_${i} >> $LOG_SINGLE_FILE
    cat ${LOG_SINGLE_SIMPLE_FILE}_${i} >> $LOG_SINGLE_SIMPLE_FILE
    rm -rf ${LOG_SINGLE_FILE}_${i} ${LOG_SINGLE_SIMPLE_FILE}_${i}
done
echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 日志合并成功，地址：$LOG_SINGLE_FILE ... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE

#多线程备份结束
echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 多线程备份数据结束... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE


#删除备份记录
if [[ -n $DEL_DAYS && $DEL_DAYS -gt 0 ]]; then
    #删除多少天之前的备份数据
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 开始删除${DEL_DAYS}天前备份数据... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-----------------------------------------------------------------------" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 开始删除备份记录目录：$DEL_PATTERN " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-----------------------------------------------------------------------" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
        
        #本地删除
        rm -rf $DEL_PATTERN 2>&1 | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
        
    echo "-----------------------------------------------------------------------" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 成功删除备份记录目录：$DEL_PATTERN ..." | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-----------------------------------------------------------------------" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 删除${DEL_DAYS}天前备份数据成功... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
fi

#FTP 上传
if [[ -n $FTP_IP && -n $FTP_USER && -n $FTP_PASSWORD && -n $FTP_BASE_DIR ]]; then
    if [ -z "$FTP_PORT" ]; then
        FTP_PORT=21
    fi
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 开始上传压缩包：$MONGODB_BACKUPDATA_DIR ... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    
    
#实现上传
createBaseDir=0
if [ "$groupNum" -gt 0 ]; then
    for ((i = 0 ; i < $groupNum ; i++))  
    do  
        db_group=${groupArr[i]}
        if [ -z "$db_group" ];then
            continue
        fi
        
        #创建目录
        if [ "$createBaseDir" -eq 0 ]; then
            ((createBaseDir++))
            
ftp -v -n $FTP_IP $FTP_PORT<<EOF
user $FTP_USER $FTP_PASSWORD
binary
hash
mkdir $FTP_DIR
bye
EOF

        fi
        
ftp -v -n $FTP_IP $FTP_PORT<<EOF
user $FTP_USER $FTP_PASSWORD
binary
hash
mkdir $FTP_DIR/$db_group
cd $FTP_DIR/$db_group
lcd $MONGODB_BACKUPDATA_DIR/$db_group
prompt
mput *
bye
EOF

    # 删除本地
    if [[ -n $IS_FTP_SAVE_ONLY && $IS_FTP_SAVE_ONLY = "true" ]];then
        rm -rf $MONGODB_BACKUPDATA_DIR/$db_group
    fi
    
    done
else

ftp -v -n $FTP_IP $FTP_PORT<<EOF
user $FTP_USER $FTP_PASSWORD
binary
hash
mkdir $FTP_DIR
cd $FTP_DIR
lcd $MONGODB_BACKUPDATA_DIR
prompt
mput *
bye
EOF

fi

    # 删除本地
    if [[ -n $IS_FTP_SAVE_ONLY && $IS_FTP_SAVE_ONLY = "true" ]];then
        rm -rf $MONGODB_BACKUPDATA_DIR
    fi
    
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 上传压缩包至FTP：$FTP_DIR ... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
fi

#发送邮件
if [ -n "$TO_EMAIL" ]; then
    if [ -z "$EMAIL_SUBJECT" ]; then
        EMAIL_SUBJECT="数据库备份($TIME_FORMAT_2)"
    fi
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 发送备份邮件至($TO_EMAIL)... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    #echo "-----------------------------------------------------------------------" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
        #cat $LOG_SINGLE_FILE | mail -s $EMAIL_SUBJECT $TO_EMAIL
    #echo "-----------------------------------------------------------------------" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 邮件发送成功... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo "=======================================================================" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
    echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
fi


#结束 
STOP_TIME=`date +%s`
USED_TIME=`expr $STOP_TIME - $START_TIME`
USED_TIME_STR=$(echo $USED_TIME | awk '{t=split("60 秒 60 分 24 时 999 天",a); \

for(n=1;n<t;n+=2){ \

if ($1==0) break; \

s=$1%a[n]a[n+1]s; \

$1=int($1/a[n]) } \

print s }')                       
echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "***********************************************************************" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') 执行${operation_type}备份数据库结束，耗时(${USED_TIME_STR})... " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo "***********************************************************************" | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE
echo " " | tee -a $LOG_SINGLE_FILE | tee -a $LOG_SINGLE_SIMPLE_FILE

#发送邮件
if [ -n "$TO_EMAIL" ]; then
	TO_EMAILS=${TO_EMAIL//// }
    for singleEMAIL in $TO_EMAILS   
    do  
        cat $LOG_SINGLE_SIMPLE_FILE | mail -s $EMAIL_SUBJECT -a $LOG_SINGLE_FILE $singleEMAIL
    done
fi

# 归档日志
touch $LOG_FILE
cat $LOG_SINGLE_FILE >> $LOG_FILE
rm -rf $LOG_SINGLE_FILE $LOG_SINGLE_SIMPLE_FILE

# 回到原目录
cd $CUR_DIR
